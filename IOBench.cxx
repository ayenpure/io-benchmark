#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/Initialize.h>
#include <vtkm/cont/RuntimeDeviceTracker.h>
#include <vtkm/cont/Timer.h>

#include <vtkm/filter/geometry_refinement/Tetrahedralize.h>
#include <vtkm/filter/vector_analysis/Gradient.h>

#include "boost/program_options.hpp"

#include "PerlinNoise.h"

namespace options = boost::program_options;

namespace config
{

class Config
{
public:
  Config() = default;

  void SetCycles(const vtkm::Id& cycles) {this->Cycles = cycles;}
  vtkm::Id GetCycles() const {return this->Cycles;}

  void SetDimensions(const vtkm::Id& dims) {this->Dimensions = dims;}
  vtkm::Id GetDimensions() const {return this->Dimensions;}

  void SetStructured(bool isStructured) {this->Structured = isStructured;}
  bool GetStructured() const {return this->Structured;}

  void SetPartitioned(bool isPartitioned) {this->Partitioned = isPartitioned;}
  bool GetPartitioned() const {return this->Partitioned;}

private:
  vtkm::Id Cycles;
  vtkm::Id Dimensions;
  bool Structured;
  bool Partitioned;
};

int ValidateOptions(options::variables_map& vm,
                    config::Config& config)
{
  if(!vm.count("cycles"))
    return -1;
  config.SetCycles(vm["cycles"].as<vtkm::Id>());
  if(!vm.count("dims"))
    return -1;
  config.SetDimensions(vm["dims"].as<vtkm::Id>());
  if(!vm.count("structured"))
    return -1;
  config.SetStructured(vm["structured"].as<bool>());
  if(!vm.count("partitioned"))
    return -1;
  config.SetPartitioned(vm["paritioned"].as<bool>());

  return 0;
}

} //namespace Config

namespace
{

// The input datasets we'll use on the filters:
vtkm::cont::DataSet InputDataSet;
vtkm::cont::PartitionedDataSet PartitionedInputDataSet;
// The point scalars to use:
std::string PointScalarsName;
// The point vectors to use:
std::string PointVectorsName;

std::vector<vtkm::cont::DataSet> ExtractDataSets(const vtkm::cont::PartitionedDataSet& partitions)
{
  return partitions.GetPartitions();
}

// Mirrors ExtractDataSet(ParitionedDataSet), to keep code simple at use sites
std::vector<vtkm::cont::DataSet> ExtractDataSets(vtkm::cont::DataSet& dataSet)
{
  return std::vector<vtkm::cont::DataSet>{ dataSet };
}

void BuildInputDataSet(vtkm::Id cycle, const config::Config& config)
{
  bool isStructured = config.GetStructured();
  bool isMultiBlock = config.GetPartitioned();
  vtkm::Id dim      = config.GetDimensions();

  vtkm::cont::PartitionedDataSet partitionedInputDataSet;
  vtkm::cont::DataSet inputDataSet;

  PointScalarsName = "perlinnoise";
  PointVectorsName = "perlinnoisegrad";

  // Generate uniform dataset(s)
  vtkm::source::PerlinNoise noise;
  noise.SetPointDimensions({ dim, dim, dim });
  noise.SetSeed(static_cast<vtkm::IdComponent>(cycle));
  if (isMultiBlock)
  {
    for (auto i = 0; i < 2; ++i)
    {
      for (auto j = 0; j < 2; ++j)
      {
        for (auto k = 0; k < 2; ++k)
        {
          noise.SetOrigin({ static_cast<vtkm::FloatDefault>(i),
                            static_cast<vtkm::FloatDefault>(j),
                            static_cast<vtkm::FloatDefault>(k) });
          const auto dataset = noise.Execute();
          partitionedInputDataSet.AppendPartition(dataset);
        }
      }
    }
  }
  else
  {
    inputDataSet = noise.Execute();
  }

  // Generate Perln Noise Gradient point vector field
  vtkm::filter::vector_analysis::Gradient gradientFilter;
  gradientFilter.SetActiveField(PointScalarsName, vtkm::cont::Field::Association::Points);
  gradientFilter.SetComputePointGradient(true);
  gradientFilter.SetOutputFieldName(PointVectorsName);
  gradientFilter.SetFieldsToPass(
    vtkm::filter::FieldSelection(vtkm::filter::FieldSelection::Mode::All));
  if (isMultiBlock)
  {
    partitionedInputDataSet = gradientFilter.Execute(partitionedInputDataSet);
  }
  else
  {
    inputDataSet = gradientFilter.Execute(inputDataSet);
  }

  // Run Tetrahedralize filter to convert uniform dataset(s) into unstructured ones
  if (!isStructured)
  {
    vtkm::filter::geometry_refinement::Tetrahedralize destructizer;
    destructizer.SetFieldsToPass(
      vtkm::filter::FieldSelection(vtkm::filter::FieldSelection::Mode::All));
    if (isMultiBlock)
    {
      partitionedInputDataSet = destructizer.Execute(partitionedInputDataSet);
    }
    else
    {
      inputDataSet = destructizer.Execute(inputDataSet);
    }
  }

  // Release execution resources to simulate in-situ workload, where the data is
  // not present in the execution environment
  std::vector<vtkm::cont::DataSet> dataSets =
    isMultiBlock ? ExtractDataSets(partitionedInputDataSet) : ExtractDataSets(inputDataSet);
  for (auto& dataSet : dataSets)
  {
    dataSet.GetCellSet().ReleaseResourcesExecution();
    dataSet.GetCoordinateSystem().ReleaseResourcesExecution();
    dataSet.GetField(PointScalarsName).ReleaseResourcesExecution();
    dataSet.GetField(PointVectorsName).ReleaseResourcesExecution();
  }

  PartitionedInputDataSet = partitionedInputDataSet;
  InputDataSet = inputDataSet;
}

} // namespace

int main(int argc, char* argv[])
{

  namespace options = boost::program_options;
  options::options_description desc("Options");
  desc.add_options()("dims",        options::value<vtkm::Id>()->required(),   "Dimension in each direction")
                    ("structured",  options::value<bool>()->required(),       "Make strucutred dataset (tetrahedralize otherwise)")
                    ("partitioned", options::value<bool>()->required(),       "Make Partitioned dataset");
  options::variables_map vm;
  options::store( options::parse_command_line(argc, argv, desc), vm);
  options::notify(vm);

  config::Config config;
  int res = config::ValidateOptions(vm, config);
  if(res < 0)
  {
    std::cout << "Error : required options not provided : " << std::endl << desc << std::endl;
    exit(EXIT_FAILURE);
  }

  // If we need to pass cycles, as in a temporal simulation,
  // change the hardcoded `0`
  BuildInputDataSet(vtkm::IdComponent(0), config);


  //TODO : write ADIOS/FIDES code here


  return 0;
}
